import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;

public class DeleteProduct extends JFrame {
	JPanel panel;
	JLabel datelbl, medicinecode, medicinename, medicinebrand,
			medicationstorage, medicationnumber, medicineUnit, medicineprices,
			medicinenote, medicineprofile, iconmedicine;
	JTextField idtxt, nametxt, pricetxt, txtmedicinecode, txtmedicinename,
			txtmedicinebrand, txtmedicationstorage, txtmedicationnumber,
			txtmedicineUnit, txtmedicineprices, txtmedicinenote;
	JButton rptbtn, savebtn, resetbtn;
	Icon medicine;
	product prod = new product();
	product pr[] = new product[9999];
	AddPanel p;
	Font fn = new Font("TAHOMA", Font.PLAIN, 18);
	JTextArea textArea;
	JDialog sub;
	JScrollPane spWords;
	String pcode, pname, pbrand, pstorage, punit, pnote, ccodec, cnamec, csex,
			cblood, cage, caddress, ccall, cnotec;
	float pprices, pnumber, pqty;
	int count = 0;
	int rec;

	public DeleteProduct(String title) {
		setTitle(title);
		setSize(600, 700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		buildPanel();
		add(panel);
		setVisible(true);
	}

	private void buildPanel() {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		medicine = new ImageIcon("medicine.jpg");
		iconmedicine = new JLabel(medicine, SwingConstants.LEFT);
		medicineprofile = new JLabel("แก้ไขรายละเอียดของยา");
		medicinecode = new JLabel("รหัสยา");
		medicinename = new JLabel("ชื่อสามัญยา");
		medicinebrand = new JLabel("บริษัท");
		medicationstorage = new JLabel("ที่เก็บ");
		medicationnumber = new JLabel("จำนวน");
		medicineUnit = new JLabel("หน่วย");
		medicineprices = new JLabel("ราคา");
		medicinenote = new JLabel("หมายเหตุ");

		Date DateName = new Date();
		SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
		String dd = df.format(DateName.getTime());
		datelbl = new JLabel("Date : " + dd + "   ");

		txtmedicinecode = new JTextField(15);
		txtmedicinename = new JTextField(15);
		txtmedicinebrand = new JTextField(15);
		txtmedicationstorage = new JTextField(15);
		txtmedicationnumber = new JTextField(15);
		txtmedicineUnit = new JTextField(15);
		txtmedicineprices = new JTextField(15);
		txtmedicinenote = new JTextField(15);

		rptbtn = new JButton("Report");
		rptbtn.setForeground(Color.blue);
		savebtn = new JButton(" Save ");
		savebtn.setForeground(Color.blue);
		resetbtn = new JButton("Delete ");
		resetbtn.setForeground(Color.blue);
		rptbtn.addActionListener(new ButtonListener());
		savebtn.addActionListener(new ButtonListener());
		resetbtn.addActionListener(new ButtonListener());

		medicineprofile.setFont(fn);
		medicinecode.setFont(fn);
		medicinename.setFont(fn);
		medicinebrand.setFont(fn);
		medicationstorage.setFont(fn);
		medicationnumber.setFont(fn);
		medicineUnit.setFont(fn);
		medicineprices.setFont(fn);
		medicinenote.setFont(fn);
		txtmedicinecode.setFont(fn);
		txtmedicinename.setFont(fn);
		txtmedicinebrand.setFont(fn);
		txtmedicationstorage.setFont(fn);
		txtmedicationnumber.setFont(fn);
		txtmedicineUnit.setFont(fn);
		txtmedicineprices.setFont(fn);
		txtmedicinenote.setFont(fn);

		rptbtn.setFont(fn);
		savebtn.setFont(fn);
		resetbtn.setFont(fn);

		p = new AddPanel();
		p.addItem(panel, iconmedicine, 0, 0, 1, 3, GridBagConstraints.WEST);
		p.addItem(panel, medicineprofile, 1, 1, 2, 1, GridBagConstraints.WEST);
		p.addItem(panel, medicinecode, 0, 3, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinecode, 1, 3, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicinename, 0, 4, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinename, 1, 4, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicinebrand, 0, 5, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinebrand, 1, 5, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicationstorage, 0, 6, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicationstorage, 1, 6, 2, 1,
				GridBagConstraints.EAST);
		p.addItem(panel, medicationnumber, 0, 7, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicationnumber, 1, 7, 2, 1,
				GridBagConstraints.EAST);
		p.addItem(panel, medicineUnit, 0, 8, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicineUnit, 1, 8, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicineprices, 0, 9, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicineprices, 1, 9, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicinenote, 0, 10, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinenote, 1, 10, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, rptbtn, 0, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, savebtn, 1, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, resetbtn, 2, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, datelbl, 1, 12, 3, 1, GridBagConstraints.EAST);

		txtmedicinecode.addActionListener(new TextListener());
	}

	private class TextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String id;
			if (e.getSource() == txtmedicinecode) {
				id = txtmedicinecode.getText();
				count = 0;
				if (!checkID(id)) {
					JOptionPane.showMessageDialog(null,
							"ไม่พบข้อมูลของรหัสสินค้า " + id + " !!!");
					txtmedicinecode.setText("");
					txtmedicinename.setText("");
					txtmedicinebrand.setText("");
					txtmedicationstorage.setText("");
					txtmedicationnumber.setText("");
					txtmedicineUnit.setText("");
					txtmedicineprices.setText("");
					txtmedicinenote.setText("");
					txtmedicinecode.requestFocus();
				}
			}
		}
	}

	private boolean checkID(String id) {
		boolean found = false;
		pr = prod.readdata("product.txt");
		for (int j = 0; j < prod.rec; j++) {
			if (pr[j].code.equals(id)) {
				pcode = pr[j].code;
				pname = pr[j].name;
				pbrand = pr[j].brand;
				pstorage = pr[j].storage;
				pprices = pr[j].prices;
				punit = pr[j].unit;
				pnumber = pr[j].number;
				pnote = pr[j].note;
				DecimalFormat fm = new DecimalFormat("#,##0.00");
				txtmedicinename.setText(pname);
				txtmedicinebrand.setText(pbrand);
				txtmedicationstorage.setText(pstorage);
				txtmedicationnumber.setText(fm.format(pnumber));
				txtmedicineUnit.setText(punit);
				txtmedicineprices.setText(fm.format(pprices));
				txtmedicinenote.setText(pnote);
				found = true;
			}
		}
		return found;
	}

	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String c, n, b, s, u, no;
			float p, num;
			if (e.getSource() == rptbtn) {
				textArea = new JTextArea(100, 100);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				pr = prod.readdata("product.txt");
				sub = new JDialog();
				sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
				sub.setSize(1000, 500);
				textArea.setText("         				 	Product List\n\n");
				textArea.append("รหัสยา" + "	" + "ชื่อสามัญยา" + "	"
						+ "บริษัท" + "			" + "ที่เก็บ" + "			"
						+ "จำนวน" + "	" + "หน่วย" + "	"
						+ "ราคา" + "	" + "หมายเหตุ" + "\n\n");
				for (int j = 0; j < prod.rec; j++) {
					textArea.append(pr[j].code + "	" + pr[j].name + "		"
							+ pr[j].brand + "			" + pr[j].storage + "			"
							+ pr[j].number + "	" + pr[j].unit + "	"
							+  pr[j].prices+ "	" + pr[j].note + "\n");
				}
				textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
				textArea.setForeground(Color.blue);
				spWords = new JScrollPane(textArea);
				spWords.setPreferredSize(new Dimension(1000, 500));
				sub.add(spWords);
				sub.setLocationRelativeTo(null);
				sub.setVisible(true);
			}
			if (e.getSource() == savebtn) {
				c = txtmedicinecode.getText();
				n = txtmedicinename.getText();
				b = txtmedicinebrand.getText();
				s = txtmedicationstorage.getText();
				num = new Float(txtmedicationnumber.getText());
				u = txtmedicineUnit.getText();
				p = new Float(txtmedicineprices.getText());
				no = txtmedicinenote.getText();
				
				String id = null;
				id = txtmedicinecode.getText();
				if (!checkID1(id)) {
				}
				prod.addData("bin/product.txt", c, n, b, s, num, u, p, no);
				deleteDir(new File("product.txt"));
				File file = new File("bin/product.txt");
				String destination = "product.txt";
				if(file.renameTo(new File(destination))){
				System.out.println("Move success");
				}else{
				System.out.println("Move failed");
				}
			deleteDir(new File("bin/product.txt"));
				txtmedicinecode.setText("");
				txtmedicinename.setText("");
				txtmedicinebrand.setText("");
				txtmedicationstorage.setText("");
				txtmedicationnumber.setText("");
				txtmedicineUnit.setText("");
				txtmedicineprices.setText("");
				txtmedicinenote.setText("");

				txtmedicinecode.requestFocus();
			}
			if (e.getSource() == resetbtn) {
				String id = null;
				id = txtmedicinecode.getText();
				if (!checkID1(id)) {
				}
				deleteDir(new File("product.txt"));
				File file = new File("bin/product.txt");
				String destination = "product.txt";
				if(file.renameTo(new File(destination))){
				System.out.println("Move success");
				}else{
				System.out.println("Move failed");
				}
			deleteDir(new File("bin/product.txt"));
			txtmedicinecode.setText("");
			txtmedicinename.setText("");
			txtmedicinebrand.setText("");
			txtmedicationstorage.setText("");
			txtmedicationnumber.setText("");
			txtmedicineUnit.setText("");
			txtmedicineprices.setText("");
			txtmedicinenote.setText("");
			txtmedicinecode.requestFocus();
			}
		}
	}

	private boolean checkID1(String id) {
		pr = prod.readdata("product.txt");
		for (int j = 0; j < prod.rec; j++) {
			if (pr[j].code.equals(id)) {}
			else {
			pcode = pr[j].code;
			pname = pr[j].name;
			pbrand = pr[j].brand;
			pstorage = pr[j].storage;
			pprices = pr[j].prices;
			punit = pr[j].unit;
			pnumber = pr[j].number;
			pnote = pr[j].note;
			prod.addData("bin/product.txt", pcode, pname, pbrand, pstorage, pnumber, punit, pprices, pnote);
		}
		}
		return true;
	}

	private static void deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] subDirs = dir.list();
			for (int i = 0; i < subDirs.length; i++) {
				deleteDir(new File(dir, subDirs[i]));
			}
		}
		dir.delete();
	}

}
