import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddProduct extends JFrame {
	JPanel panel;
	JLabel datelbl, medicinecode, medicinename, medicinebrand,
			medicationstorage, medicationnumber, medicineUnit, medicineprices,
			medicinenote, medicineprofile, iconmedicine;
	JTextField idtxt, nametxt, pricetxt, txtmedicinecode, txtmedicinename,
			txtmedicinebrand, txtmedicationstorage, txtmedicationnumber,
			txtmedicineUnit, txtmedicineprices, txtmedicinenote;
	JButton rptbtn, savebtn, resetbtn;
	Icon medicine;
	product prod = new product();
	product pr[] = new product[9999];
	AddPanel p;
	Font fn = new Font("TAHOMA", Font.PLAIN, 18);
	JTextArea textArea;
	JDialog sub;
	JScrollPane spWords;

	public AddProduct(String title) {
		setTitle(title);
		setSize(600, 700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		buildPanel();
		add(panel);
		setVisible(true);
	}

	private void buildPanel() {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		medicine = new ImageIcon("medicine.jpg");
		iconmedicine = new JLabel(medicine, SwingConstants.LEFT);
		medicineprofile = new JLabel("รายละเอียดของยา");
		medicinecode = new JLabel("รหัสยา");
		medicinename = new JLabel("ชื่อสามัญยา");
		medicinebrand = new JLabel("บริษัท");
		medicationstorage = new JLabel("ที่เก็บ");
		medicationnumber = new JLabel("จำนวน");
		medicineUnit = new JLabel("หน่วย");
		medicineprices = new JLabel("ราคา");
		medicinenote = new JLabel("หมายเหตุ");

		Date DateName = new Date();
		SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
		String dd = df.format(DateName.getTime());
		datelbl = new JLabel("Date : " + dd + "   ");

		txtmedicinecode = new JTextField(15);
		txtmedicinename = new JTextField(15);
		txtmedicinebrand = new JTextField(15);
		txtmedicationstorage = new JTextField(15);
		txtmedicationnumber = new JTextField(15);
		txtmedicineUnit = new JTextField(15);
		txtmedicineprices = new JTextField(15);
		txtmedicinenote = new JTextField(15);

		rptbtn = new JButton("Report");
		rptbtn.setForeground(Color.blue);
		savebtn = new JButton(" Save ");
		savebtn.setForeground(Color.blue);
		resetbtn = new JButton("Reset ");
		resetbtn.setForeground(Color.blue);
		rptbtn.addActionListener(new ButtonListener());
		savebtn.addActionListener(new ButtonListener());
		resetbtn.addActionListener(new ButtonListener());

		medicineprofile.setFont(fn);
		medicinecode.setFont(fn);
		medicinename.setFont(fn);
		medicinebrand.setFont(fn);
		medicationstorage.setFont(fn);
		medicationnumber.setFont(fn);
		medicineUnit.setFont(fn);
		medicineprices.setFont(fn);
		medicinenote.setFont(fn);
		txtmedicinecode.setFont(fn);
		txtmedicinename.setFont(fn);
		txtmedicinebrand.setFont(fn);
		txtmedicationstorage.setFont(fn);
		txtmedicationnumber.setFont(fn);
		txtmedicineUnit.setFont(fn);
		txtmedicineprices.setFont(fn);
		txtmedicinenote.setFont(fn);

		rptbtn.setFont(fn);
		savebtn.setFont(fn);
		resetbtn.setFont(fn);

		p = new AddPanel();
		p.addItem(panel, iconmedicine, 0, 0, 1, 3, GridBagConstraints.WEST);
		p.addItem(panel, medicineprofile, 1, 1, 2, 1, GridBagConstraints.WEST);
		p.addItem(panel, medicinecode, 0, 3, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinecode, 1, 3, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicinename, 0, 4, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinename, 1, 4, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicinebrand, 0, 5, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinebrand, 1, 5, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicationstorage, 0, 6, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicationstorage, 1, 6, 2, 1,
				GridBagConstraints.EAST);
		p.addItem(panel, medicationnumber, 0, 7, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicationnumber, 1, 7, 2, 1,
				GridBagConstraints.EAST);
		p.addItem(panel, medicineUnit, 0, 8, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicineUnit, 1, 8, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicineprices, 0, 9, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicineprices, 1, 9, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, medicinenote, 0, 10, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtmedicinenote, 1, 10, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, rptbtn, 0, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, savebtn, 1, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, resetbtn, 2, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, datelbl, 1, 12, 3, 1, GridBagConstraints.EAST);
	}

	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String c, n, b, s, u, no;
			float p, num;
			if (e.getSource() == rptbtn) {
				textArea = new JTextArea(100, 100);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				pr = prod.readdata("product.txt");
				sub = new JDialog();
				sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
				sub.setSize(1000, 500);
				textArea.setText("         				 	Product List\n\n");
				textArea.append("รหัสยา" + "	" + "ชื่อสามัญยา" + "	"
						+ "บริษัท" + "			" + "ที่เก็บ" + "			"
						+ "จำนวน" + "	" + "หน่วย" + "	"
						+ "ราคา" + "	" + "หมายเหตุ" + "\n\n");
				for (int j = 0; j < prod.rec; j++) {
					textArea.append(pr[j].code + "	" + pr[j].name + "		"
							+ pr[j].brand + "			" + pr[j].storage + "			"
							+ pr[j].number + "	" + pr[j].unit + "	"
							+  pr[j].prices+ "	" + pr[j].note + "\n");
				}
				textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
				textArea.setForeground(Color.blue);
				spWords = new JScrollPane(textArea);
				spWords.setPreferredSize(new Dimension(1000, 500));
				sub.add(spWords);
				sub.setLocationRelativeTo(null);
				sub.setVisible(true);
			}
			if (e.getSource() == savebtn) {
				c = txtmedicinecode.getText();
				n = txtmedicinename.getText();
				b = txtmedicinebrand.getText();
				s = txtmedicationstorage.getText();
				num = new Float(txtmedicationnumber.getText());
				u = txtmedicineUnit.getText();
				p = new Float(txtmedicineprices.getText());
				no = txtmedicinenote.getText();

				prod.addData("product.txt", c, n, b, s, num, u, p, no);
				txtmedicinecode.setText("");
				txtmedicinename.setText("");
				txtmedicinebrand.setText("");
				txtmedicationstorage.setText("");
				txtmedicationnumber.setText("");
				txtmedicineUnit.setText("");
				txtmedicineprices.setText("");
				txtmedicinenote.setText("");

				txtmedicinecode.requestFocus();
			}
			if (e.getSource() == resetbtn) {
				txtmedicinecode.setText("");
				txtmedicinename.setText("");
				txtmedicinebrand.setText("");
				txtmedicationstorage.setText("");
				txtmedicationnumber.setText("");
				txtmedicineUnit.setText("");
				txtmedicineprices.setText("");
				txtmedicinenote.setText("");

			}
		}
	}
}
