import java.io.*;
public class datapassword {
	String user,pass;
	int rec4;
	void addData4(String fn, String user, String pass) {
	    File fname = new File(fn);
	    FileOutputStream f_out = null;
	    BufferedOutputStream b_out = null;
	    DataOutputStream d_out = null;
	    try {
	      f_out = new FileOutputStream(fname,true);
	      b_out = new BufferedOutputStream(f_out);
	      d_out = new DataOutputStream(b_out);
	      d_out.writeUTF(user);
	      d_out.writeUTF(pass);
	    }
	    catch (Exception e) {
	      System.out.println(e);
	    }
	    finally {
	      try {
	        if (d_out != null)
	          d_out.close();
	      }
	      catch (IOException e){
	        System.out.println(e);
	      }
	    }
	  }
	datapassword[] readdata(String fn) {
	    File ifile = new File(fn);
	    FileInputStream f_in = null;
	    BufferedInputStream b_in = null;
	    DataInputStream d_in = null;
	    boolean eof = false;
	    datapassword[] pw = new datapassword[9999];
	    try {
	      f_in = new FileInputStream(ifile);
	      b_in = new BufferedInputStream(f_in);
	      d_in = new DataInputStream(b_in);
	      rec4 = 0;
	      while (!eof) {
	    	  pw[rec4] = new datapassword();
	    	  pw[rec4].user = d_in.readUTF();
	    	  pw[rec4].pass = d_in.readUTF();
	        rec4++;
	      }
	    }
	    catch (EOFException e){
	      eof = true;
	    }
	    catch (FileNotFoundException e){
	      return null;
	    }
	    catch (IOException e){
	      return null;
	    }
	    finally {
	      try {
	        if (d_in != null)
	          d_in.close();
	      }
	      catch (IOException e){
	        System.out.println(e);
	      }
	    }
	    return pw;
	  }
	}
