import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductSale extends JFrame {
	JPanel panel;
	JLabel salemedicine, anslbl;
	JLabel datelbl, medicinecode, medicinename, medicinebrand,
			medicationstorage, medicationnumber, medicineUnit, medicineprices,
			medicinenote, medicinesaleprofile, iconsalemedicine, customerscode,
			customersname, customerssex, customersblood, customersage,
			customersaddress, customerscall, customersnote, iconcustomers;
	JTextField qtxt, txtmedicinecode, txtmedicinename, txtmedicinebrand,
			txtmedicationstorage, txtmedicationnumber, txtmedicineUnit,
			txtmedicineprices, txtmedicinenote, txtcustomerscode,
			txtcustomersname, txtcustomerssex, txtcustomersblood,
			txtcustomersage, txtcustomersaddress, txtcustomerscall,
			txtcustomersnote;
	Icon medicinesale, customers;
	JButton rptbtn, savebtn, resetbtn;
	Font fn = new Font("TAHOMA", Font.PLAIN, 18);
	Font tfn = new Font("TH Sarabun New", Font.BOLD, 24);
	AddPanel y;
	product prod = new product();
	product pr[] = new product[9999];
	saledata saled = new saledata();
	saledata sd[] = new saledata[9999];
	customers custo = new customers();
	customers cu[] = new customers[9999];
	int count = 0;
	String pcode, pname, pbrand, pstorage, punit, pnote, ccodec, cnamec, csex,
			cblood, cage, caddress, ccall, cnotec;
	float pprices, pnumber, pqty;
	JTextArea textArea;
	JDialog sub;
	JScrollPane spWords;
	String dd;
	float total2 = 0f,pqty2 = 0f;

	public ProductSale(String title) {
		setTitle(title);
		setSize(900, 700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		buildPanel();
		add(panel);
		setVisible(true);
	}

	private void buildPanel() {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		medicinesale = new ImageIcon("medicine.jpg");
		iconsalemedicine = new JLabel(medicinesale, SwingConstants.LEFT);
		medicinesaleprofile = new JLabel("ขายสินค้า");
		medicinecode = new JLabel("รหัสยา");
		medicinename = new JLabel("ชื่อสามัญยา");
		medicinebrand = new JLabel("บริษัท");
		medicationstorage = new JLabel("ที่เก็บ");
		medicationnumber = new JLabel("จำนวนที่เหลือ");
		medicineUnit = new JLabel("หน่วย");
		medicineprices = new JLabel("ราคา");
		medicinenote = new JLabel("หมายเหตุ");

		customers = new ImageIcon("customers.jpg");
		iconcustomers = new JLabel(customers, SwingConstants.LEFT);
		customerscode = new JLabel("รหัสลูกค้า");
		customersname = new JLabel("ชื่อ-สกุล ลูกค้า");
		customerssex = new JLabel("เพศ");
		customersblood = new JLabel("หมู่เลือด");
		customersage = new JLabel("อายุ");
		customersaddress = new JLabel("ที่อยู่");
		customerscall = new JLabel("เบอร์โทร");
		customersnote = new JLabel("ข้อมูลสุขภาพ");

		Date DateName = new Date();
		SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
		dd = df.format(DateName.getTime());
		datelbl = new JLabel("Date : " + dd + "   ");

		salemedicine = new JLabel("ขายเป็นจำนวน");
		qtxt = new JTextField(15);

		txtmedicinecode = new JTextField(15);
		txtmedicinename = new JTextField(15);
		txtmedicinename.setEditable(false);
		txtmedicinebrand = new JTextField(15);
		txtmedicinebrand.setEditable(false);
		txtmedicationstorage = new JTextField(15);
		txtmedicationstorage.setEditable(false);
		txtmedicationnumber = new JTextField(15);
		txtmedicationnumber.setEditable(false);
		txtmedicineUnit = new JTextField(15);
		txtmedicineUnit.setEditable(false);
		txtmedicineprices = new JTextField(15);
		txtmedicineprices.setEditable(false);
		txtmedicinenote = new JTextField(15);
		txtmedicinenote.setEditable(false);

		txtcustomerscode = new JTextField(15);
		txtcustomersname = new JTextField(15);
		txtcustomerssex = new JTextField(15);
		txtcustomersblood = new JTextField(15);
		txtcustomersage = new JTextField(15);
		txtcustomersaddress = new JTextField(15);
		txtcustomerscall = new JTextField(15);
		txtcustomersnote = new JTextField(15);
		txtcustomersname.setEditable(false);
		txtcustomerssex.setEditable(false);
		txtcustomersblood.setEditable(false);
		txtcustomersage.setEditable(false);
		txtcustomersaddress.setEditable(false);
		txtcustomerscall.setEditable(false);
		txtcustomersnote.setEditable(false);

		medicinesaleprofile.setFont(fn);
		medicinecode.setFont(fn);
		medicinename.setFont(fn);
		medicinebrand.setFont(fn);
		medicationstorage.setFont(fn);
		medicationnumber.setFont(fn);
		medicineUnit.setFont(fn);
		medicineprices.setFont(fn);
		medicinenote.setFont(fn);
		txtmedicinecode.setFont(fn);
		txtmedicinename.setFont(fn);
		txtmedicinebrand.setFont(fn);
		txtmedicationstorage.setFont(fn);
		txtmedicationnumber.setFont(fn);
		txtmedicineUnit.setFont(fn);
		txtmedicineprices.setFont(fn);
		txtmedicinenote.setFont(fn);
		salemedicine.setFont(fn);
		qtxt.setFont(fn);

		customerscode.setFont(fn);
		customersname.setFont(fn);
		customerssex.setFont(fn);
		customersblood.setFont(fn);
		customersage.setFont(fn);
		customersaddress.setFont(fn);
		customerscall.setFont(fn);
		customersnote.setFont(fn);
		txtcustomerscode.setFont(fn);
		txtcustomersname.setFont(fn);
		txtcustomerssex.setFont(fn);
		txtcustomersblood.setFont(fn);
		txtcustomersage.setFont(fn);
		txtcustomersaddress.setFont(fn);
		txtcustomerscall.setFont(fn);
		txtcustomersnote.setFont(fn);

		rptbtn = new JButton("Report");
		rptbtn.setForeground(Color.blue);
		savebtn = new JButton(" Save ");
		savebtn.setForeground(Color.blue);
		resetbtn = new JButton("Reset ");
		resetbtn.setForeground(Color.blue);

		anslbl = new JLabel("", SwingConstants.CENTER);
		anslbl.setOpaque(true);
		anslbl.setBackground(Color.blue);
		anslbl.setForeground(Color.white);
		anslbl.setPreferredSize(new Dimension(500, 30));

		rptbtn.setFont(fn);
		savebtn.setFont(fn);
		resetbtn.setFont(fn);
		anslbl.setFont(tfn);

		txtmedicinecode.addActionListener(new TextListener());
		txtcustomerscode.addActionListener(new TextListener());
		qtxt.addActionListener(new TextListener());
		rptbtn.addActionListener(new ButtonListener());
		savebtn.addActionListener(new ButtonListener());
		resetbtn.addActionListener(new ButtonListener());

		y = new AddPanel();
		y.addItem(panel, iconsalemedicine, 2, 0, 1, 3, GridBagConstraints.WEST);
		y.addItem(panel, medicinesaleprofile, 3, 2, 2, 1,
				GridBagConstraints.WEST);

		y.addItem(panel, medicinecode, 3, 3, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicinecode, 4, 3, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, medicinename, 3, 4, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicinename, 4, 4, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, medicinebrand, 3, 5, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicinebrand, 4, 5, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, medicationstorage, 3, 6, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicationstorage, 4, 6, 2, 1,
				GridBagConstraints.EAST);
		y.addItem(panel, medicationnumber, 3, 7, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicationnumber, 4, 7, 2, 1,
				GridBagConstraints.EAST);
		y.addItem(panel, medicineUnit, 3, 8, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicineUnit, 4, 8, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, medicineprices, 3, 9, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicineprices, 4, 9, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, medicinenote, 3, 10, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtmedicinenote, 4, 10, 2, 1, GridBagConstraints.EAST);

		y.addItem(panel, customerscode, 0, 3, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomerscode, 1, 3, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, customersname, 0, 4, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomersname, 1, 4, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, customerssex, 0, 5, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomerssex, 1, 5, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, customersblood, 0, 6, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomersblood, 1, 6, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, customersage, 0, 7, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomersage, 1, 7, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, customersaddress, 0, 8, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomersaddress, 1, 8, 2, 1,
				GridBagConstraints.EAST);
		y.addItem(panel, customerscall, 0, 9, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomerscall, 1, 9, 2, 1, GridBagConstraints.EAST);
		y.addItem(panel, customersnote, 0, 10, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, txtcustomersnote, 1, 10, 2, 1, GridBagConstraints.EAST);

		y.addItem(panel, salemedicine, 3, 11, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, qtxt, 4, 11, 2, 1, GridBagConstraints.EAST);

		y.addItem(panel, rptbtn, 1, 12, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, savebtn, 2, 12, 1, 1, GridBagConstraints.WEST);
		y.addItem(panel, resetbtn, 4, 12, 1, 1, GridBagConstraints.WEST);

		y.addItem(panel, anslbl, 1, 13, 4, 1, GridBagConstraints.CENTER);

		y.addItem(panel, datelbl, 4, 14, 3, 1, GridBagConstraints.EAST);
	}

	private class TextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String id, id2;
			String cc, nc, c, n, b, s, u,no, item;
			float p, num,num1,p1;
			if (e.getSource() == txtmedicinecode) {
				id = txtmedicinecode.getText();
				count = 0;
				if (!checkID(id)) {
					JOptionPane.showMessageDialog(null,
							"ไม่พบข้อมูลของรหัสสินค้า " + id + " !!!");
					txtmedicinecode.setText("");
					txtmedicinename.setText("");
					txtmedicinebrand.setText("");
					txtmedicationstorage.setText("");
					txtmedicationnumber.setText("");
					txtmedicineUnit.setText("");
					txtmedicineprices.setText("");
					txtmedicinenote.setText("");
					qtxt.setText("");
					anslbl.setText("");
					txtmedicinecode.requestFocus();
				}
			}
			if (e.getSource() == txtcustomerscode) {
				id2 = txtcustomerscode.getText();
				count = 0;
				if (!checkID2(id2)) {
					JOptionPane.showMessageDialog(null, "ไม่พบข้อมูลของลูกค้า "
							+ id2 + " !!!");
					txtcustomerscode.setText("");
					txtcustomersname.setText("");
					txtcustomerssex.setText("");
					txtcustomersblood.setText("");
					txtcustomersage.setText("");
					txtcustomersaddress.setText("");
					txtcustomerscall.setText("");
					txtcustomersnote.setText("");
					txtcustomerscode.requestFocus();
				}
			}
			if (e.getSource() == qtxt) {
				pqty = new Float(qtxt.getText());
				float total1 = pqty * pprices;
				pqty2 = pqty2 + pqty;
				total2 = total2+total1;
				DecimalFormat fm = new DecimalFormat("#,##0.00");
				String ans = "รวมเป็นเงิน " + fm.format(total2) + " บาท ";
				anslbl.setText(ans);
				cc = txtcustomerscode.getText();
				nc = txtcustomersname.getText();
				c = txtmedicinecode.getText();
				n = txtmedicinename.getText();
				num = new Float(qtxt.getText());
				p = pqty * pprices;
				saled.addData3("sale.txt", cc, nc, c, n, num, p, dd);
				b = txtmedicinebrand.getText();
				s = txtmedicationstorage.getText();
				num1 = new Float(txtmedicationnumber.getText());
				u = txtmedicineUnit.getText();
				p1 = new Float(txtmedicineprices.getText());
				no = txtmedicinenote.getText();
				id = txtmedicinecode.getText();
				if (!checkID1(id)) {
				}
				prod.addData("bin/product.txt", c, n, b, s, num1-num, u, p1, no);
				deleteDir(new File("product.txt"));
				File file = new File("bin/product.txt");
				String destination = "product.txt";
				if(file.renameTo(new File(destination))){
				System.out.println("Move success");
				}else{
				System.out.println("Move failed");
				}
				deleteDir(new File("bin/product.txt"));
				txtmedicinecode.setText("");
				txtmedicinename.setText("");
				txtmedicinebrand.setText("");
				txtmedicationstorage.setText("");
				txtmedicationnumber.setText("");
				txtmedicineUnit.setText("");
				txtmedicineprices.setText("");
				txtmedicinenote.setText("");
				qtxt.setText("");
				txtmedicinecode.requestFocus();
			}
		}
	}

	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String cc, nc, c, n, b, s, u, item;
			float p, num;
			if (e.getSource() == rptbtn) {
				textArea = new JTextArea(100, 100);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				sd = saled.readdata("sale.txt");
				sub = new JDialog();
				sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
				sub.setSize(1200, 500);
				textArea.setText("         				 Product Sale List\n\n");
				textArea.append("รหัสลูกค้า" + "	" + "ชื่อ-สกุล ลูกค้า" + "		"
						+ "รหัสยา" + "	" + "ชื่อสามัญยา" + "		"+ "จำนวนสินค้า" + "	" + "จำนวนเงิน"
						+ "		" + "วันที่ขาย" + "\n\n");
				for (int j = 0; j < saled.rec3; j++) {
					textArea.append(sd[j].codec + "	" + sd[j].namec + "			"
							+ sd[j].code + "	" + sd[j].name + "			"
							+ sd[j].numitem+"		"+ sd[j].pricessale + "		" + sd[j].day + "\n");
				}
				textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
				textArea.setForeground(Color.blue);
				spWords = new JScrollPane(textArea);
				spWords.setPreferredSize(new Dimension(1200, 500));
				sub.add(spWords);
				sub.setLocationRelativeTo(null);
				sub.setVisible(true);
			}

			if (e.getSource() == savebtn) {
				cc = txtcustomerscode.getText();
				nc = txtcustomersname.getText();
				c = "-";
				n = "-";
				num = pqty2;
				p = total2;
				saled.addData3("sale.txt", cc, nc, c, n, num, p, dd+"		รวม"+"\n");
				txtcustomerscode.setText("");
				txtcustomersname.setText("");
				txtcustomerssex.setText("");
				txtcustomersblood.setText("");
				txtcustomersage.setText("");
				txtcustomersaddress.setText("");
				txtcustomerscall.setText("");
				txtcustomersnote.setText("");
				txtmedicinecode.setText("");
				txtmedicinename.setText("");
				txtmedicinebrand.setText("");
				txtmedicationstorage.setText("");
				txtmedicationnumber.setText("");
				txtmedicineUnit.setText("");
				txtmedicineprices.setText("");
				txtmedicinenote.setText("");
				qtxt.setText("");
				anslbl.setText("");
				txtcustomerscode.requestFocus();
				total2 = 0f;

			}
			if (e.getSource() == resetbtn) {
				txtmedicinecode.setText("");
				txtmedicinename.setText("");
				txtmedicinebrand.setText("");
				txtmedicationstorage.setText("");
				txtmedicationnumber.setText("");
				txtmedicineUnit.setText("");
				txtmedicineprices.setText("");
				txtmedicinenote.setText("");
				qtxt.setText("");
				anslbl.setText("");
				txtmedicinecode.requestFocus();
			}
		}
	}

	private boolean checkID(String id) {
		boolean found = false;
		pr = prod.readdata("product.txt");
		for (int j = 0; j < prod.rec; j++) {
			if (pr[j].code.equals(id)) {
				pcode = pr[j].code;
				pname = pr[j].name;
				pbrand = pr[j].brand;
				pstorage = pr[j].storage;
				pprices = pr[j].prices;
				punit = pr[j].unit;
				pnumber = pr[j].number;
				pnote = pr[j].note;
				DecimalFormat fm = new DecimalFormat("#,##0.00");
				txtmedicinename.setText(pname);
				txtmedicinebrand.setText(pbrand);
				txtmedicationstorage.setText(pstorage);
				txtmedicationnumber.setText(fm.format(pnumber));
				txtmedicineUnit.setText(punit);
				txtmedicineprices.setText(fm.format(pprices));
				txtmedicinenote.setText(pnote);
				qtxt.requestFocus();
				found = true;
			}
		}
		return found;
	}

	private boolean checkID2(String id) {
		boolean found = false;
		cu = custo.readdata("customers.txt");
		for (int j = 0; j < custo.rec2; j++) {
			if (cu[j].codec.equals(id)) {
				ccodec = cu[j].codec;
				cnamec = cu[j].namec;
				csex = cu[j].sex;
				cblood = cu[j].blood;
				cage = cu[j].age;
				caddress = cu[j].address;
				ccall = cu[j].call;
				cnotec = cu[j].notec;
				txtcustomersname.setText(cnamec);
				txtcustomerssex.setText(csex);
				txtcustomersblood.setText(cblood);
				txtcustomersage.setText(cage);
				txtcustomersaddress.setText(caddress);
				txtcustomerscall.setText(ccall);
				txtcustomersnote.setText(cnotec);
				txtmedicinecode.requestFocus();
				found = true;
			}
		}
		return found;
	}
	private boolean checkID1(String id) {
		pr = prod.readdata("product.txt");
		for (int j = 0; j < prod.rec; j++) {
			if (pr[j].code.equals(id)) {}
			else {
			pcode = pr[j].code;
			pname = pr[j].name;
			pbrand = pr[j].brand;
			pstorage = pr[j].storage;
			pprices = pr[j].prices;
			punit = pr[j].unit;
			pnumber = pr[j].number;
			pnote = pr[j].note;
			prod.addData("bin/product.txt", pcode, pname, pbrand, pstorage, pnumber, punit, pprices, pnote);
		}
		}
		return true;
	}

	private static void deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] subDirs = dir.list();
			for (int i = 0; i < subDirs.length; i++) {
				deleteDir(new File(dir, subDirs[i]));
			}
		}
		dir.delete();
	}
}
