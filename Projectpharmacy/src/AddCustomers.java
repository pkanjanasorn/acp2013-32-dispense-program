// คลาสที่เอาไว้ใช้รับข้อมูลลูกค้า
import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;
public class AddCustomers extends JFrame {
	JPanel panel;
	JLabel  datelbl, customerscode, customersname, customerssex,
			customersblood, customersage, customersaddress, customerscall,
			customersnote, customersprofile,iconcustomers;
	JTextField txtcustomerscode, txtcustomersname, txtcustomerssex,
	txtcustomersblood, txtcustomersage, txtcustomersaddress, txtcustomerscall,
	txtcustomersnote;
	JButton rptbtn, savebtn, resetbtn;
	Icon customers;
	customers custo = new customers();
	customers cu[] = new customers[9999];
	AddPanel p;
	Font fn = new Font("TAHOMA", Font.PLAIN, 18);
	JTextArea textArea;
	JDialog sub;
	JScrollPane spWords;

	public AddCustomers(String title) {
		setTitle(title);
		setSize(600, 700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		buildPanel();
		add(panel);
		setVisible(true);
	}

	private void buildPanel() {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		customers = new ImageIcon("customers.jpg");
		iconcustomers = new JLabel(customers, SwingConstants.LEFT);
		customersprofile = new JLabel("รายละเอียดลูกค้า");
		customerscode = new JLabel("รหัสลูกค้า");
		customersname = new JLabel("ชื่อ-สกุล ลูกค้า");
		customerssex = new JLabel("เพศ");
		customersblood = new JLabel("หมู่เลือด");
		customersage = new JLabel("อายุ");
		customersaddress = new JLabel("ที่อยู่");
		customerscall = new JLabel("เบอร์โทร");
		customersnote = new JLabel("ข้อมูลสุขภาพ");
		
		Date DateName = new Date();
		SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
		String dd = df.format(DateName.getTime());
		datelbl = new JLabel("Date : " + dd + "   ");

		txtcustomerscode = new JTextField(15);
		txtcustomersname = new JTextField(15);
		txtcustomerssex = new JTextField(15);
		txtcustomersblood = new JTextField(15);
		txtcustomersage = new JTextField(15);
		txtcustomersaddress = new JTextField(15);
		txtcustomerscall = new JTextField(15);
		txtcustomersnote = new JTextField(15);

		rptbtn = new JButton("Report");
		rptbtn.setForeground(Color.blue);
		savebtn = new JButton(" Save ");
		savebtn.setForeground(Color.blue);
		resetbtn = new JButton("Reset ");
		resetbtn.setForeground(Color.blue);
		rptbtn.addActionListener(new ButtonListener());
		savebtn.addActionListener(new ButtonListener());
		resetbtn.addActionListener(new ButtonListener());

		customersprofile.setFont(fn);
		customerscode.setFont(fn);
		customersname.setFont(fn);
		customerssex.setFont(fn);
		customersblood.setFont(fn);
		customersage.setFont(fn);
		customersaddress.setFont(fn);
		customerscall.setFont(fn);
		customersnote.setFont(fn);
		txtcustomerscode.setFont(fn);
		txtcustomersname.setFont(fn);
		txtcustomerssex.setFont(fn);
		txtcustomersblood.setFont(fn);
		txtcustomersage.setFont(fn);
		txtcustomersaddress.setFont(fn);
		txtcustomerscall.setFont(fn);
		txtcustomersnote.setFont(fn);

		rptbtn.setFont(fn);
		savebtn.setFont(fn);
		resetbtn.setFont(fn);

		p = new AddPanel();
		p.addItem(panel, iconcustomers, 0, 0, 1, 3, GridBagConstraints.WEST);
		p.addItem(panel, customersprofile, 1, 1, 2, 1, GridBagConstraints.WEST);
		p.addItem(panel, customerscode, 0, 3, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomerscode, 1, 3, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customersname, 0, 4, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomersname, 1, 4, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customerssex, 0, 5, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomerssex, 1, 5, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customersblood, 0, 6, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomersblood, 1, 6, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customersage, 0, 7, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomersage, 1, 7, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customersaddress, 0, 8, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomersaddress, 1, 8, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customerscall, 0, 9, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomerscall, 1, 9, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, customersnote, 0, 10, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, txtcustomersnote, 1, 10, 2, 1, GridBagConstraints.EAST);
		p.addItem(panel, rptbtn, 0, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, savebtn, 1, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, resetbtn, 2, 11, 1, 1, GridBagConstraints.WEST);
		p.addItem(panel, datelbl, 1, 12, 3, 1, GridBagConstraints.EAST);
}

	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String c,n,s,b,a,ad,ca,no;
			if (e.getSource() == rptbtn) {//ทำการแสดงข้อมูลที่เคยเซฟไว้
				textArea = new JTextArea(100, 100);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				cu = custo.readdata("customers.txt");
				sub = new JDialog();
				sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
				sub.setSize(1300, 500);
				textArea.setText("         					 Customers List\n\n");
				textArea.append("รหัสลูกค้า" + "	" + "ชื่อ-สกุล ลูกค้า" + "		"
						+ "เพศ" + "	" + "หมู่เลือด" + "	"
						+ "อายุ" + "	" + "ที่อยู่" + "			"
						+ "เบอร์โทร" + "		" + "ข้อมูลสุขภาพ" + "\n\n");
				for (int j = 0; j < custo.rec2; j++){
					textArea.append(cu[j].codec + "	" + cu[j].namec + "			"
							+ cu[j].sex + "	" + cu[j].blood + "	"
							+ cu[j].age + "	" + cu[j].address + "			"
							+ cu[j].call + "		" + cu[j].notec + "\n");
				}
				textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
				textArea.setForeground(Color.blue);
				spWords = new JScrollPane(textArea);
				spWords.setPreferredSize(new Dimension(1300, 500));
				sub.add(spWords);
				sub.setLocationRelativeTo(null);
				sub.setVisible(true);
			}
			if (e.getSource() == savebtn) {
				c=txtcustomerscode.getText();
				n=txtcustomersname.getText();
				s=txtcustomerssex.getText();
				b=txtcustomersblood.getText();
				a=txtcustomerscall.getText();
				ad=txtcustomersaddress.getText();
				ca=txtcustomersage.getText();
				no=txtcustomersnote.getText();

				custo.addData2("customers.txt", c,n,s,b,ca,ad,a,no);//เมื่อกดเซฟแล้วจะทำการส่งข้อมูลไปเก็บไว้ที่คลาสcustomers
				txtcustomerscode.setText("");
				txtcustomersname.setText("");
				txtcustomerssex.setText("");
				txtcustomersblood.setText("");
				txtcustomersage.setText("");
				txtcustomersaddress.setText("");
				txtcustomerscall.setText("");
				txtcustomersnote.setText("");

				txtcustomerscode.requestFocus();
			}
			if (e.getSource() == resetbtn) {
				txtcustomerscode.setText("");
				txtcustomersname.setText("");
				txtcustomerssex.setText("");
				txtcustomersblood.setText("");
				txtcustomersage.setText("");
				txtcustomersaddress.setText("");
				txtcustomerscall.setText("");
				txtcustomersnote.setText("");

			}
		}
	}
}

