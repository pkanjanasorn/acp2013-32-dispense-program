import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class Framelogin extends JFrame {
	JPanel panel; JLabel ulbl, pwlbl ,icongear;
	  JTextField usertxt; JPasswordField pwtxt;
	  JButton signbtn, resetbtn, closebtn; Icon gear;
	  AddPanel p; 
	  Font fn = new Font("Estrangelo Edessa",Font.PLAIN,18);
	  datapassword pass = new datapassword();
	  datapassword pw[] = new datapassword[9999];
	  String userold, passwordold;
	  JPanel panel1 = new JPanel();
	  public Framelogin(String title) {
	    setTitle(title);
	    setSize(475, 352);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    buildPanel();
	    add(panel);
	    setVisible(true);
	  }
	  private void buildPanel() { 
	    panel = new JPanel();
	    panel.setLayout(new GridBagLayout()); 	    
	   // panel.setBorder(BorderFactory.createLineBorder(Color.WHITE));
	   // panel.setBackground(Color.WHITE);
	    
	    gear= new ImageIcon("gear.jpg");
	    icongear = new JLabel(gear, SwingConstants.LEFT);
	    ulbl = new JLabel("username");
	    pwlbl = new JLabel("password");
	    usertxt = new JTextField(10);
	    pwtxt = new JPasswordField(10);
	     
	    signbtn = new JButton(" sign in ");
	    signbtn.setForeground(Color.blue);
	    signbtn.setBackground(Color.WHITE);
	    resetbtn = new JButton("  reset  ");
	    resetbtn.setForeground(Color.blue);
	    resetbtn.setBackground(Color.WHITE);
	    closebtn = new JButton("  Close  ");
	    closebtn.setForeground(Color.red);
	    closebtn.setBackground(Color.WHITE);
	   
	    ulbl.setFont(fn);       usertxt.setFont(fn); 
	    pwlbl.setFont(fn);      pwtxt.setFont(fn);
	    signbtn.setFont(fn);    resetbtn.setFont(fn);   closebtn.setFont(fn);       
	    
	    p = new AddPanel();
	   // p.addItem(panel,icongear,1,0,3,1,GridBagConstraints.NORTH);
	    p.addItem(panel,ulbl,0,2,3,1,GridBagConstraints.WEST);
	    p.addItem(panel,usertxt,3,2,3,1,GridBagConstraints.WEST);
	    p.addItem(panel,pwlbl,0,3,3,1,GridBagConstraints.WEST);
	    p.addItem(panel,pwtxt,3,3,3,1,GridBagConstraints.WEST);    
	    p.addItem(panel,signbtn,0,4,2,1,GridBagConstraints.WEST);  
	    p.addItem(panel,resetbtn,2,4,2,1,GridBagConstraints.WEST);
	    p.addItem(panel,closebtn,4,4,2,1,GridBagConstraints.EAST);
	    
	    signbtn.addActionListener(new ButtonListener());
	    resetbtn.addActionListener(new ButtonListener());
	    closebtn.addActionListener(new ButtonListener());
	  }
	  private class ButtonListener implements ActionListener {
	    @Override  
	    public void actionPerformed(ActionEvent e) {
	      String user, password;   
	      if (e.getSource()==signbtn) {
		user = usertxt.getText();			     
		password = pwtxt.getText();
		if (!checkID5(user)||!checkID6(password)) {JOptionPane.showMessageDialog(null, "Login Fail; Try again !!!");
        usertxt.setText("");
        pwtxt.setText("");}
		else if (user.equals(userold) && password.equals(passwordold)) {
		  MainProjectpharmacy  s = new MainProjectpharmacy("หน้าหลัก");
		}
	        usertxt.setText("");
		pwtxt.setText("");
	      }
	      if (e.getSource()==resetbtn)  {
	        usertxt.setText("");
		pwtxt.setText("");
	       }
	      if(e.getSource()==closebtn)  {
				System.exit(0);
			      }
	    }		
	  }
	  private boolean checkID5(String id) {
			boolean found = false;
			pw = pass.readdata("datapassword.txt");
			for (int j = 0; j < pass.rec4; j++) {
				if (pw[j].user.equals(id)) {
					userold = pw[j].user;
					passwordold = pw[j].pass; 							
					found = true;
				}
			}
			return found;
		}
	  private boolean checkID6(String id) {
			boolean found = false;
			pw = pass.readdata("datapassword.txt");
			for (int j = 0; j < pass.rec4; j++) {
				if (pw[j].pass.equals(id)) {
					userold = pw[j].user;
					passwordold = pw[j].pass; 							
					found = true;
				}
			}
			
			return found;
		}
	  public static void main(String[] args)  {
		    try {
				//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		    	UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		    	//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		    	//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
		    	//UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
		    	//UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			} catch (Throwable e) {
				e.printStackTrace();
			}
		  Framelogin s = new Framelogin("เข้าสู่ระบบ");
	  }
}