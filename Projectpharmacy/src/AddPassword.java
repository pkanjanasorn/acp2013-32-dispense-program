import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.io.File;
import java.text.SimpleDateFormat;

public class AddPassword extends JFrame {
	JPanel panel;
	JLabel ulbl, ulbl1, pwlbl, pwlbl1, pwlbl2, x, y;
	JTextField usertxt, usertxt1;
	JPasswordField pwtxt, pwtxt1, pwtxt2;
	JButton savebtn, closebtn;
	AddPanel p;
	Font fn = new Font("Estrangelo Edessa", Font.PLAIN, 18);
	String user, password, user1, password1, password2;
	datapassword pass = new datapassword();
	datapassword pw[] = new datapassword[9999];

	public AddPassword(String title) {
		setTitle(title);
		setSize(475, 352);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		buildPanel();
		add(panel);
		setVisible(true);
	}

	private void buildPanel() {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		ulbl = new JLabel("usernameold");
		pwlbl = new JLabel("passwordold");
		ulbl1 = new JLabel("newusername");
		pwlbl1 = new JLabel("newpasswordold");
		pwlbl2 = new JLabel("confirm password");
		usertxt = new JTextField(10);
		pwtxt = new JPasswordField(10);
		usertxt1 = new JTextField(10);
		usertxt1.setEditable(false);
		pwtxt1 = new JPasswordField(10);
		pwtxt1.setEditable(false);
		pwtxt2 = new JPasswordField(10);
		pwtxt2.setEditable(false);
		x = new JLabel("=========================");
		y = new JLabel("=========================");

		savebtn = new JButton(" Save");
		savebtn.setForeground(Color.blue);
		closebtn = new JButton("  Close  ");
		closebtn.setForeground(Color.red);

		ulbl.setFont(fn);
		usertxt.setFont(fn);
		pwlbl.setFont(fn);
		pwtxt.setFont(fn);
		ulbl1.setFont(fn);
		usertxt1.setFont(fn);
		pwlbl1.setFont(fn);
		pwtxt1.setFont(fn);
		pwtxt2.setFont(fn);
		pwlbl2.setFont(fn);
		savebtn.setFont(fn);
		closebtn.setFont(fn);

		p = new AddPanel();
		p.addItem(panel, ulbl, 0, 2, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, usertxt, 3, 2, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, pwlbl, 0, 3, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, pwtxt, 3, 3, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, x, 0, 4, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, y, 3, 4, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, ulbl1, 0, 5, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, usertxt1, 3, 5, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, pwlbl1, 0, 6, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, pwtxt1, 3, 6, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, pwlbl2, 0, 7, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, pwtxt2, 3, 7, 3, 1, GridBagConstraints.WEST);
		p.addItem(panel, savebtn, 0, 8, 2, 1, GridBagConstraints.WEST);
		p.addItem(panel, closebtn, 4, 8, 2, 1, GridBagConstraints.EAST);

		savebtn.addActionListener(new ButtonListener());
		closebtn.addActionListener(new ButtonListener());
		pwtxt.addActionListener(new TextListener());
	}

	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == savebtn) {
				user1 = usertxt1.getText();
				password1 = pwtxt1.getText();
				password2 = pwtxt2.getText();
				if (user1.equals("") && password1.equals("")
						&& password2.equals("")) {
					JOptionPane.showMessageDialog(null,
							"Do not use a blank password reset; Try again !!!");
				} else if (password1.equals(password2)) {
					pass.addData4("bin/datapassword.txt", user1, password1);
					deleteDir(new File("datapassword.txt"));
					File file = new File("bin/datapassword.txt");
					String destination = "datapassword.txt";
					if (file.renameTo(new File(destination))) {
						System.out.println("Move success");
					} else {
						System.out.println("Move failed");
					}
					deleteDir(new File("bin/datapassword.txt"));
				} else {
					JOptionPane.showMessageDialog(null,
							"New passwords mismatch; Try again !!!");
				}
				usertxt1.setText("");
				pwtxt1.setText("");
				pwtxt2.setText("");
				usertxt1.requestFocus();
			}
			if (e.getSource() == closebtn) {
				System.exit(0);
			}
		}
	}

	private class TextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String userold, passwordold;
			if (e.getSource() == pwtxt) {
				userold = usertxt.getText();
				passwordold = pwtxt.getText();
				if (!checkID5(userold) || !checkID6(passwordold)) {
					JOptionPane.showMessageDialog(null,
							"Password Fail; Try again !!!");
					usertxt.setText("");
					pwtxt.setText("");
				} else if (userold.equals(user) && passwordold.equals(password)) {
					usertxt1.setEditable(true);
					pwtxt1.setEditable(true);
					pwtxt2.setEditable(true);
					usertxt1.requestFocus();
				}
			}
		}
	}

	private boolean checkID5(String id) {
		boolean found = false;
		pw = pass.readdata("datapassword.txt");
		for (int j = 0; j < pass.rec4; j++) {
			if (pw[j].user.equals(id)) {
				user = pw[j].user;
				password = pw[j].pass;
				found = true;
			}
		}
		return found;
	}

	private boolean checkID6(String id) {
		boolean found = false;
		pw = pass.readdata("datapassword.txt");
		for (int j = 0; j < pass.rec4; j++) {
			if (pw[j].pass.equals(id)) {
				user = pw[j].user;
				password = pw[j].pass;
				found = true;
			}
		}
		return found;
	}

	private static void deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] subDirs = dir.list();
			for (int i = 0; i < subDirs.length; i++) {
				deleteDir(new File(dir, subDirs[i]));
			}
		}
		dir.delete();
	}

}
