import java.io.*;
public class product {
  String code,name,brand,storage,unit,note;
  float prices,number;
  int rec;
  void addData(String fn, String code, String name, String brand,String storage, float prices, String unit, float number, String note) {
    File fname = new File(fn);
    FileOutputStream f_out = null;
    BufferedOutputStream b_out = null;
    DataOutputStream d_out = null;
    try {
      f_out = new FileOutputStream(fname,true);
      b_out = new BufferedOutputStream(f_out);
      d_out = new DataOutputStream(b_out);
      d_out.writeUTF(code);
      d_out.writeUTF(name);
      d_out.writeUTF(brand);
      d_out.writeUTF(storage);
      d_out.writeFloat(number);
      d_out.writeUTF(unit);
      d_out.writeFloat(prices);
      d_out.writeUTF(note);
    }
    catch (Exception e) {
      System.out.println(e);
    }
    finally {
      try {
        if (d_out != null)
          d_out.close();
      }
      catch (IOException e){
        System.out.println(e);
      }
    }
  }
  product[] readdata(String fn) {
    File ifile = new File(fn);
    FileInputStream f_in = null;
    BufferedInputStream b_in = null;
    DataInputStream d_in = null;
    boolean eof = false;
    product[] pr = new product[9999];
    try {
      f_in = new FileInputStream(ifile);
      b_in = new BufferedInputStream(f_in);
      d_in = new DataInputStream(b_in);
      rec = 0;
      while (!eof) {
        pr[rec] = new product();
        pr[rec].code = d_in.readUTF();
        pr[rec].name = d_in.readUTF();
        pr[rec].brand = d_in.readUTF();
        pr[rec].storage = d_in.readUTF();
        pr[rec].prices = d_in.readFloat();
        pr[rec].unit = d_in.readUTF();
        pr[rec].number = d_in.readFloat();
        pr[rec].note = d_in.readUTF();
        rec++;
      }
    }
    catch (EOFException e){
      eof = true;
    }
    catch (FileNotFoundException e){
      return null;
    }
    catch (IOException e){
      return null;
    }
    finally {
      try {
        if (d_in != null)
          d_in.close();
      }
      catch (IOException e){
        System.out.println(e);
      }
    }
    return pr;
  }
}
