import java.io.*;
public class saledata {
	String codec,namec,code,name,day;
	  float numitem,pricessale;
	int rec3;
	 void addData3(String fn,String codec,String namec, String code,String name, float numitem, float pricessale ,String day) {
		    File fname = new File(fn);
		    FileOutputStream f_out = null;
		    BufferedOutputStream b_out = null;
		    DataOutputStream d_out = null;
		    try {
		      f_out = new FileOutputStream(fname,true);
		      b_out = new BufferedOutputStream(f_out);
		      d_out = new DataOutputStream(b_out);
		      d_out.writeUTF(codec);
		      d_out.writeUTF(namec);
		      d_out.writeUTF(code);
		      d_out.writeUTF(name);
		      d_out.writeFloat(numitem);
		      d_out.writeFloat(pricessale);
		      d_out.writeUTF(day);
		    }
		    catch (Exception e) {
		      System.out.println(e);
		    }
		    finally {
		      try {
		        if (d_out != null)
		          d_out.close();
		      }
		      catch (IOException e){
		        System.out.println(e);
		      }
		    }
		  }
	 saledata[] readdata(String fn) {
	    File ifile = new File(fn);
	    FileInputStream f_in = null;
	    BufferedInputStream b_in = null;
	    DataInputStream d_in = null;
	    boolean eof = false;
	    saledata[] sd = new saledata[9999];
	    try {
	      f_in = new FileInputStream(ifile);
	      b_in = new BufferedInputStream(f_in);
	      d_in = new DataInputStream(b_in);
	      rec3 = 0;
	      while (!eof) {
	    	sd[rec3] = new saledata();
	    	sd[rec3].codec = d_in.readUTF();
	    	sd[rec3].namec = d_in.readUTF();
	    	sd[rec3].code = d_in.readUTF();
	    	sd[rec3].name = d_in.readUTF();
	    	sd[rec3].numitem = d_in.readFloat();
	    	sd[rec3].pricessale = d_in.readFloat();
	    	sd[rec3].day = d_in.readUTF();
	        rec3++;
	      }
	    }
	    catch (EOFException e){
	      eof = true;
	    }
	    catch (FileNotFoundException e){
	      return null;
	    }
	    catch (IOException e){
	      return null;
	    }
	    finally {
	      try {
	        if (d_in != null)
	          d_in.close();
	      }
	      catch (IOException e){
	        System.out.println(e);
	      }
	    }
	    return sd;
	  }
	}


