import java.awt.*; 						

import javax.swing.*;					

import java.awt.event.*;
public class MainProjectpharmacy extends JFrame {
	  JPanel panel; Icon ani;
	  JButton addpr,addcus, salebtn, closebtn,delpr,delcu,product,customers,sale,addpass;  
	  AddPanel p;
	  Font fn = new Font("TAHOMA",Font.PLAIN,18);
	  JTextArea textArea;
		JDialog sub;
		JScrollPane spWords;
		customers custo = new customers();
		customers cu[] = new customers[9999];
		product prod = new product();
		product pr[] = new product[9999];
		saledata saled = new saledata();
		saledata sd[] = new saledata[9999];
		
	  public MainProjectpharmacy (String title) {
		  setTitle(title);
		  setSize(350, 300);
		  setLocationRelativeTo(null);
		  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
		  buildPanel();
		  add(panel);
		  setVisible(true);
		  }
	  private void buildPanel() {    
		    panel = new JPanel();
		    panel.setLayout(new GridBagLayout());
		    
		    sale = new JButton(" ข้อมูลการขาย  ");
		    sale.setForeground(Color.blue);
		    product= new JButton(" 	ข้อมูลสินค้า  ");
		    product.setForeground(Color.blue);
		    customers= new JButton(" ข้อมูลลูกค้า  ");
		    customers.setForeground(Color.blue);
		    addpr = new JButton("	 เพิ่มสินค้า  	");
		    addpr.setForeground(Color.blue);
		    addcus = new JButton("	 เพิ่มลูกค้า  	");
		    addcus.setForeground(Color.blue);
		    delpr = new JButton("แก้ไขข้อมูลสินค้า	");
		    delpr.setForeground(Color.blue);
		    delcu = new JButton("แก้ไขข้อมูลลูกค้า	");
		    delcu.setForeground(Color.blue);
		    salebtn = new JButton("	ขายสินค้า ");
		    salebtn.setForeground(Color.green);
		    addpass= new JButton("เปลี่ยนpassword");
		    addpass.setForeground(Color.blue);
		    closebtn = new JButton("   Close  	");
		    closebtn.setForeground(Color.red);
		    
		    sale.setBackground(Color.WHITE);
		    product.setBackground(Color.WHITE);
		    customers.setBackground(Color.WHITE);
		    addpr.setBackground(Color.WHITE);
		    addcus.setBackground(Color.WHITE);
		    delpr.setBackground(Color.WHITE);
		    delcu.setBackground(Color.WHITE);
		    salebtn.setBackground(Color.WHITE);
		    addpass.setBackground(Color.WHITE);
		    closebtn.setBackground(Color.WHITE);
		    
		    addpr.setFont(fn);
		    addcus.setFont(fn);
		    salebtn.setFont(fn);    
		    closebtn.setFont(fn);
		    delpr.setFont(fn);
		    delcu.setFont(fn);
		    product.setFont(fn);
		    customers.setFont(fn);
		    sale.setFont(fn);
		    addpass.setFont(fn);
		    
		    p = new AddPanel();   
		    p.addItem(panel,salebtn,0, 4, 1, 1,GridBagConstraints.EAST);
		    p.addItem(panel,sale,2, 4, 2, 1,GridBagConstraints.EAST);
		    p.addItem(panel,product,0, 5, 1, 1,GridBagConstraints.EAST);
		    p.addItem(panel,customers,2, 5, 2, 1,GridBagConstraints.EAST);
		    p.addItem(panel,addpr,0, 6, 1, 1,GridBagConstraints.EAST);
		    p.addItem(panel,addcus,2, 6, 2, 1,GridBagConstraints.EAST);
		    p.addItem(panel,delpr,0, 7, 1, 1,GridBagConstraints.EAST);
		    p.addItem(panel,delcu,2, 7, 2, 1,GridBagConstraints.EAST);
		    p.addItem(panel,addpass,0, 8, 1, 1,GridBagConstraints.EAST);
		    p.addItem(panel,closebtn,2, 8, 2, 1,GridBagConstraints.EAST);
		        
		    addpr.addActionListener(new ButtonListener());
		    addcus.addActionListener(new ButtonListener());
		    salebtn.addActionListener(new ButtonListener());
		    closebtn.addActionListener(new ButtonListener());	 
		    delpr.addActionListener(new ButtonListener());
		    delcu.addActionListener(new ButtonListener());
		    product.addActionListener(new ButtonListener());
		    customers.addActionListener(new ButtonListener());
		    sale.addActionListener(new ButtonListener());
		    addpass.addActionListener(new ButtonListener());
		  }
	  private class ButtonListener implements ActionListener {
		    @Override  
		    public void actionPerformed(ActionEvent e) {
		    	if (e.getSource()==sale) {
		    		textArea = new JTextArea(100, 100);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					sd = saled.readdata("sale.txt");
					sub = new JDialog();
					sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
					sub.setSize(1200, 500);
					textArea.setText("         				 Product Sale List\n\n");
					textArea.append("รหัสลูกค้า" + "	" + "ชื่อ-สกุล ลูกค้า" + "		"
							+ "รหัสยา" + "	" + "ชื่อสามัญยา" + "		"+ "จำนวนสินค้า" + "	" + "จำนวนเงิน"
							+ "		" + "วันที่ขาย" + "\n");
					for (int j = 0; j < saled.rec3; j++) {
						textArea.append(sd[j].codec + "	" + sd[j].namec + "			"
								+ sd[j].code + "	" + sd[j].name + "			"
								+ sd[j].numitem+"		"+ sd[j].pricessale + "		" + sd[j].day + "\n");
					}
					textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
					textArea.setForeground(Color.blue);
					spWords = new JScrollPane(textArea);
					spWords.setPreferredSize(new Dimension(1200, 500));
					sub.add(spWords);
					sub.setLocationRelativeTo(null);
					sub.setVisible(true);
		    	}
		    	if (e.getSource()==product) {
		    		textArea = new JTextArea(100, 100);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					pr = prod.readdata("product.txt");
					sub = new JDialog();
					sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
					sub.setSize(1000, 500);
					textArea.setText("         				 	Product List\n\n");
					textArea.append("รหัสยา" + "	" + "ชื่อสามัญยา" + "	"
							+ "บริษัท" + "			" + "ที่เก็บ" + "			"
							+ "จำนวน" + "	" + "หน่วย" + "	"
							+ "ราคา" + "	" + "หมายเหตุ" + "\n\n");
					for (int j = 0; j < prod.rec; j++) {
						textArea.append(pr[j].code + "	" + pr[j].name + "		"
								+ pr[j].brand + "			" + pr[j].storage + "			"
								+ pr[j].number + "	" + pr[j].unit + "	"
								+  pr[j].prices+ "	" + pr[j].note + "\n");
					}
					textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
					textArea.setForeground(Color.blue);
					spWords = new JScrollPane(textArea);
					spWords.setPreferredSize(new Dimension(1000, 500));
					sub.add(spWords);
					sub.setLocationRelativeTo(null);
					sub.setVisible(true);
		    	}
		    	if (e.getSource()==customers) {
		    		textArea = new JTextArea(100, 100);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					cu = custo.readdata("customers.txt");
					sub = new JDialog();
					sub.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
					sub.setSize(1300, 500);
					textArea.setText("         					 Customers List\n\n");
					textArea.append("รหัสลูกค้า" + "	" + "ชื่อ-สกุล ลูกค้า" + "		"
							+ "เพศ" + "	" + "หมู่เลือด" + "	"
							+ "อายุ" + "	" + "ที่อยู่" + "			"
							+ "เบอร์โทร" + "		" + "ข้อมูลสุขภาพ" + "\n");
					for (int j = 0; j < custo.rec2; j++){
						textArea.append(cu[j].codec + "	" + cu[j].namec + "			"
								+ cu[j].sex + "	" + cu[j].blood + "	"
								+ cu[j].age + "	" + cu[j].address + "			"
								+ cu[j].call + "		" + cu[j].notec + "\n\n");
					}
					textArea.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 16));
					textArea.setForeground(Color.blue);
					spWords = new JScrollPane(textArea);
					spWords.setPreferredSize(new Dimension(1300, 500));
					sub.add(spWords);
					sub.setLocationRelativeTo(null);
					sub.setVisible(true);
		    	}
		      if (e.getSource()==addpr) {
			AddProduct s = new AddProduct("เพิ่มสินค้า");
		      }
		      if (e.getSource()==addcus) {
		    	  AddCustomers s = new AddCustomers("เพิ่มลูกค้า");
				      }
		      if (e.getSource()==delpr) {
		    	  DeleteProduct s = new DeleteProduct("แก้ไขข้อมูลสินค้า");
				      }
		      if (e.getSource()==delcu) {
		    	  DeleteCustomers s = new DeleteCustomers("แก้ไขข้อมูลลูกค้า");
				      }
		      if (e.getSource()==salebtn) {
			ProductSale  s = new ProductSale("ขายสินค้า");
		      }
		      if (e.getSource()==addpass) {
					AddPassword s = new AddPassword("เปลี่ยนpassword");
				      }
		      if(e.getSource()==closebtn)  {
			System.exit(0);
		      }
		    }		
		   }	    	  
}
