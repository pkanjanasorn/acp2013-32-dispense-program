import java.io.*;
public class customers {
	String codec,namec,sex,blood,age,address,call,notec;
	int rec2;
	void addData2(String fn, String codec, String namec, String sex,String blood, String age, String address, String call, String notec) {
	    File fname = new File(fn);
	    FileOutputStream f_out = null;
	    BufferedOutputStream b_out = null;
	    DataOutputStream d_out = null;
	    try {
	      f_out = new FileOutputStream(fname,true);
	      b_out = new BufferedOutputStream(f_out);
	      d_out = new DataOutputStream(b_out);
	      d_out.writeUTF(codec);
	      d_out.writeUTF(namec);
	      d_out.writeUTF(sex);
	      d_out.writeUTF(blood);
	      d_out.writeUTF(age);
	      d_out.writeUTF(address);
	      d_out.writeUTF(call);
	      d_out.writeUTF(notec);
	    }
	    catch (Exception e) {
	      System.out.println(e);
	    }
	    finally {
	      try {
	        if (d_out != null)
	          d_out.close();
	      }
	      catch (IOException e){
	        System.out.println(e);
	      }
	    }
	  }
	customers[] readdata(String fn) {
	    File ifile = new File(fn);
	    FileInputStream f_in = null;
	    BufferedInputStream b_in = null;
	    DataInputStream d_in = null;
	    boolean eof = false;
	    customers[] cu = new customers[9999];
	    try {
	      f_in = new FileInputStream(ifile);
	      b_in = new BufferedInputStream(f_in);
	      d_in = new DataInputStream(b_in);
	      rec2 = 0;
	      while (!eof) {
	    	cu[rec2] = new customers();
	    	cu[rec2].codec = d_in.readUTF();
	    	cu[rec2].namec = d_in.readUTF();
	    	cu[rec2].sex = d_in.readUTF();
	    	cu[rec2].blood = d_in.readUTF();
	    	cu[rec2].age = d_in.readUTF();
	    	cu[rec2].address = d_in.readUTF();
	    	cu[rec2].call = d_in.readUTF();
	    	cu[rec2].notec = d_in.readUTF();
	        rec2++;
	      }
	    }
	    catch (EOFException e){
	      eof = true;
	    }
	    catch (FileNotFoundException e){
	      return null;
	    }
	    catch (IOException e){
	      return null;
	    }
	    finally {
	      try {
	        if (d_in != null)
	          d_in.close();
	      }
	      catch (IOException e){
	        System.out.println(e);
	      }
	    }
	    return cu;
	  }
	}